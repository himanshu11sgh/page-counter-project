from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def page_counter(request):
    count = request.session.get('count', 0)
    newcount = count+1
    request.session['count'] = newcount

    context = {
        'count': newcount
    }
    return render(request, 'core/home.html', context)

def greet(request):
    return HttpResponse("<h2>Hello World</h2>")


def greet2(request):
    return HttpResponse("<h2>Hello World2</h2>")